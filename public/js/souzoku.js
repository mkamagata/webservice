(function($){
    const inputIds = {
        'A': ['#inputANinzu4MD'],
        'B': ['#inputBGoukei4MD','#inputBKingaku14MD','#inputBKingaku24MD','#inputBKingaku34MD','#inputBKingaku44MD'],
        'C': ['#inputCGoukei4MD','#inputCKingaku14MD','#inputCKingaku24MD','#inputCKingaku34MD','#inputCKingaku44MD'],
        'D': ['#inputDGoukei4MD','#inputDKingaku14MD','#inputDKingaku24MD','#inputDKingaku34MD','#inputDKingaku44MD'],
        'E': ['#inputEGoukei4MD','#inputEKingaku14MD','#inputEKingaku24MD','#inputEKingaku34MD','#inputEKingaku44MD'],
        'F': ['#inputFGoukei4MD','#inputFKingaku14MD','#inputFKingaku24MD','#inputFKingaku34MD','#inputFKingaku44MD'],
        'G': ['#inputGGoukei4MD','#inputGKingaku14MD','#inputGKingaku24MD','#inputGKingaku34MD','#inputGKingaku44MD'],
        'H': ['#inputHGoukei4MD','#inputHKingaku14MD','#inputHKingaku24MD','#inputHKingaku34MD','#inputHKingaku44MD'],
        'I': ['#inputIGoukei4MD'],
        'J': ['#inputJGoukei4MD'],
        'K': ['#inputKGoukei4MD'],
        'L': ['#inputLGoukei4MD'],
        'M': ['#inputMGoukei4MD']
    }

    function modalAlert(message1,message2){
        $('#modal_message1').text(message1);
        $('#modal_message2').text(message2);
        $('#centralModalWarning').modal('show');
    }

    function calcA(){
        let value = null;
        if($(inputIds['A'][0]).val() !== undefined && $(inputIds['A'][0]).val() !== ""){
            value = Number.parseInt($(inputIds['A'][0]).val());
            if(value <= 0){
                modalAlert('A. 相続人の人数', '1以上の数値を入力してください！！！');
                return;
            }
        }
    }

    function calcAll(calcType){
        let sum = null;
        if($(inputIds[calcType][1]).val() !== undefined && $(inputIds[calcType][1]).val() !== ""){
            let value = Number.parseInt($(inputIds[calcType][1]).val());
            if(value <= 0){
                modalAlert(calcType, '1以上の数値を入力してください！！！');
                return;
            }
            sum += value;
        }
        if($(inputIds[calcType][2]).val() !== undefined && $(inputIds[calcType][2]).val() !== ""){
            let value = Number.parseInt($(inputIds[calcType][2]).val());
            if(value <= 0){
                modalAlert(calcType, '1以上の数値を入力してください！！！');
                return;
            }
            sum += value;
        }
        if($(inputIds[calcType][3]).val() !== undefined && $(inputIds[calcType][3]).val() !== ""){
            let value = Number.parseInt($(inputIds[calcType][3]).val());
            if(value <= 0){
                modalAlert(calcType, '1以上の数値を入力してください！！！');
                return;
            }
            sum += value;
        }
        if($(inputIds[calcType][4]).val() !== undefined && $(inputIds[calcType][4]).val() !== ""){
            let value = Number.parseInt($(inputIds[calcType][4]).val());
            if(value <= 0){
                modalAlert(calcType, '1以上の数値を入力してください！！！');
                return;
            }
            sum += value;
        }
        $(inputIds[calcType][0]).val(sum).change();
    }

    function calcI(){
        let sum = null;
        if($(inputIds['B'][0]).val() !== undefined && $(inputIds['B'][0]).val() !== ""){
            sum += Number.parseInt($(inputIds['B'][0]).val());
        }
        if($(inputIds['C'][0]).val() !== undefined && $(inputIds['C'][0]).val() !== ""){
            sum += Number.parseInt($(inputIds['C'][0]).val());
        }
        if($(inputIds['D'][0]).val() !== undefined && $(inputIds['D'][0]).val() !== ""){
            sum += Number.parseInt($(inputIds['D'][0]).val());
        }
        if($(inputIds['E'][0]).val() !== undefined && $(inputIds['E'][0]).val() !== ""){
            sum += Number.parseInt($(inputIds['E'][0]).val());
        }
        if($(inputIds['F'][0]).val() !== undefined && $(inputIds['F'][0]).val() !== ""){
            sum += Number.parseInt($(inputIds['F'][0]).val());
        }
        $(inputIds['I'][0]).val(sum).change();

    }

    function calcJ(){
        let subtract = null;
        if($(inputIds['H'][0]).val() !== undefined && $(inputIds['H'][0]).val() !== "" ||
            $(inputIds['I'][0]).val() !== undefined && $(inputIds['I'][0]).val() !== ""
        ){
            let valueH = $(inputIds['H'][0]).val();
            let valueI = $(inputIds['I'][0]).val();
            if(valueH === undefined || valueH === ""){
                valueH = '0';
            }
            if(valueI === undefined || valueI === ""){
                valueI = '0';
            }
            subtract = Number.parseInt(valueI) - Number.parseInt(valueH);
            if(subtract < 0){
                subtract = 0;
            }
        }
        $(inputIds['J'][0]).val(subtract).change();

    }

    function calcK(){
        let sum = null;
        if($(inputIds['G'][0]).val() !== undefined && $(inputIds['G'][0]).val() !== ""){
            sum += Number.parseInt($(inputIds['G'][0]).val());
        }
        if($(inputIds['J'][0]).val() !== undefined && $(inputIds['J'][0]).val() !== ""){
        
            sum += Number.parseInt($(inputIds['J'][0]).val());
        }
        $(inputIds['K'][0]).val(sum).change();
    }

    function calcL(){
        let deduction = null;
        if($(inputIds['A'][0]).val() !== undefined && $(inputIds['A'][0]).val() !== ""){
            deduction = 3000 + (Number.parseInt($(inputIds['A'][0]).val()) * 600);
        }
        $(inputIds['L'][0]).val(deduction).change();
    }

    function calcM(){
        let subtract = null;
        if($(inputIds['K'][0]).val() !== undefined && $(inputIds['K'][0]).val() !== "" ||
            $(inputIds['L'][0]).val() !== undefined && $(inputIds['L'][0]).val() !== ""
        ){
            let valueK = $(inputIds['K'][0]).val();
            let valueL = $(inputIds['L'][0]).val();
            if(valueK === undefined || valueK === ""){
                valueK = '0';
            }
            if(valueL === undefined || valueL === ""){
                valueL = '0';
            }
            subtract = Number.parseInt(valueK) - Number.parseInt(valueL);
        }
        $(inputIds['M'][0]).val(subtract).change();
    }

    function judgment(){
        if($(inputIds['A'][0]).val() !== undefined && $(inputIds['A'][0]).val() !== "" &&
            $(inputIds['I'][0]).val() !== undefined && $(inputIds['I'][0]).val() !== "" &&
            $(inputIds['M'][0]).val() !== undefined && $(inputIds['M'][0]).val() !== ""
        ){
            $('button#judg').prop('disabled', false);

        }else{

            $('button#judg').prop('disabled', true);
        }
    }

    // 判定ボタンアクション
    $('button#judg').on('click',function(){
        let value = $(inputIds['M'][0]).val();
        if(value < 0){
            $('#centralModalSuccess').modal('show');
        }else{
            $('#centralModalDanger').modal('show');
        }
        
    })

    // A.相続人の人数取得
    $('#inputANinzu4MD').on('change',function(){
        calcA();
    });

    // B. 故人名義の不動産
    $('#inputBKingaku14MD,#inputBKingaku24MD,#inputBKingaku34MD,#inputBKingaku44MD').on('change',function(){
        calcAll('B');
    });

    // C. 個人が所有していた株式、公社債、投資信託など
    $('#inputCKingaku14MD,#inputCKingaku24MD,#inputCKingaku34MD,#inputCKingaku44MD').on('change',function(){
        calcAll('C');
    });

    // D. 故人の現金、預貯金について、死亡日の残高
    $('#inputDKingaku14MD,#inputDKingaku24MD,#inputDKingaku34MD,#inputDKingaku44MD').on('change',function(){
        calcAll('D');
    });

    // E. 相続人、相続人以外の人が受け取った生命保険金、損害保険金や死亡退職金
    $('#inputEKingaku14MD,#inputEKingaku24MD,#inputEKingaku34MD,#inputEKingaku44MD').on('change',function(){
        calcAll('E');
    });
    
    // F. 故人から、相続時精算課税を適用した財産の贈与を受けた人がいる場合は、その財産
    $('#inputFKingaku14MD,#inputFKingaku24MD,#inputFKingaku34MD,#inputFKingaku44MD').on('change',function(){
        calcAll('F');
    });

    // G. 故人から、亡くなる前3年以内に、上の「F」以外の財産贈与を受けた人が居る場合は、その財産
    $('#inputGKingaku14MD,#inputGKingaku24MD,#inputGKingaku34MD,#inputGKingaku44MD').on('change',function(){
        calcAll('G');
    });
    
    // H. 故人に債務、未納の税金がある場合、その債務
    $('#inputHKingaku14MD,#inputHKingaku24MD,#inputHKingaku34MD,#inputHKingaku44MD').on('change',function(){
        calcAll('H');
    });

    // I. B + C + D + E + F の金額
    $('#inputBGoukei4MD,#inputCGoukei4MD,#inputDGoukei4MD,#inputEGoukei4MD,#inputFGoukei4MD').on('change',function(){
        calcI();
    });
    
    // J. I - H の金額
    $('#inputHGoukei4MD,#inputIGoukei4MD').on('change',function(){
        calcJ();
    });

    // K. J + G の金額
    $('#inputGGoukei4MD,#inputJGoukei4MD').on('change',function(){
        calcK();
    });

    // L. 基礎控除の計算
    $('#inputANinzu4MD').on('change',function(){
        calcL();
    });

    // M. K - L の金額
    $('#inputKGoukei4MD,#inputLGoukei4MD').on('change',function(){
        calcM();
    });

    // 判定
    $('#inputMGoukei4MD').on('change',function(){
        judgment();
    });

})(jQuery);