const express = require('express');
const ejs = require('ejs');
const moment = require('moment');
moment.locale('ja');

const app = express();
app.engine('ejs', ejs.renderFile);
app.use(express.static('public'));

app.get('/', (req, res) => {
    res.render('index.ejs',
        { title: 'Tool Box', content: 'ちょっとしたツールの保存場所です。' }
    );
});

app.get('/inheritanceTax', (req, res) => {
    res.render('inheritanceTax.ejs',
        { title: '相続税簡易判定シート', content: '相続税が掛かるケースなのか簡易判定します。' }
    );
});

/**
 * From～Toの経過日数を返す
 * @param {String} fromDate 
 * @param {String} toDate
 */
function getElapsedTime(fromDate, toDate) {
    // 形式チェック
    if(!(/^\d{8}$/).test(fromDate) || !(/^\d{8}$/).test(toDate)){
        return 'パラメータ形式不正（from=YYYYMMDD&to=YYYYMMDD）';
    }
    var fromMoment = moment(fromDate);
    var toMoment = moment(toDate);
    // 日付チェック
    if(!fromMoment.isValid() || !toMoment.isValid()){
        return '不正な日付';
    }
    // 日付の差をミリ秒で取得
    var diff = toMoment.diff(fromMoment);
    // 差分ミリ秒からdurationオブジェクト作成
    var duration = moment.duration(diff);
    // durationオブジェクトで差分の年・月・日を取得
    var outText = duration.years() + '年' + duration.months() + 'ヶ月' + duration.days() + '日 経過';
    return outText;
}

/**
 * 基準日から何日経過したか算出してJSONで返却
 */
app.get('/api/keika', (req, res) => {
    var fromDate = req.query.from;
    var toDate = req.query.to;
    var elapsedTimeMsg = getElapsedTime(fromDate, toDate);
    res.header('Access-Control-Allow-Origin', '*');
    res.send({ message: elapsedTimeMsg });
});

app.listen(process.env.PORT || 8080, () => {
    console.log('Start Server Port:8080');
});

